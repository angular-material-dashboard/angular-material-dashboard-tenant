# AMD tenant 

[![pipeline status](https://gitlab.com/angular-material-dashboard/angular-material-dashboard-tenant/badges/master/pipeline.svg)](https://gitlab.com/angular-material-dashboard/angular-material-dashboard-tenant/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/0ad28bf1a3144714a34ccc4e11e61c1b)](https://www.codacy.com/app/mostafa.barmshory/angular-material-dashboard-tenant?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-dashboard/angular-material-dashboard-tenant&amp;utm_campaign=Badge_Grade)

A module to add tenant management to angular-material-dashboard. 

## Install

This is a bower module and you can install it as follow:

	bower install --save angular-material-dashboard-tenant

It is better to address the repository directly

	bower install --save https://gitlab.com/angular-material-dashboard/angular-material-dashboard-tenant.git

## Document

- [Reference document](https://angular-material-dashboard.gitlab.io/angular-material-dashboard-tenant/doc/index.html)