/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardTenant', [ //
    'ngMaterialDashboard',//
    'seen-supertenant'//
]);

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardTenant').config(function ($routeProvider) {
    $routeProvider
        .when('/tenant/tenants', {
            templateUrl: 'views/amd-tenant-tenants.html',
            controller: 'AmdTenantTenantsController',
            controllerAs: 'ctrl',
            groups: ['tenant'],
            navigate: true,
            /*
             * @ngInject
             */
            protect: function ($rootScope) {
                return !$rootScope.account.permissions.tenant_owner;
            },
            loginRequired: true,
            name: 'Tenants',
            icon: 'business'
        })
        .when('/tenant/tenants/:tenantId', {
            templateUrl: 'views/amd-tenant-tenant.html',
            controller: 'AmdTenantTenantController',
            controllerAs: 'ctrl'
        })
        .when('/tenant/tickets', {
            templateUrl: 'views/amd-tenant/ticket-list.html',
            controller: 'AmdTenantTicketsController',
            groups: ['tenant'],
            navigate: true,
            name: 'Tickets',
            icon: 'question_answer',
            /*
             * @ngInject
             */
            protect: function ($rootScope) {
                return !$rootScope.app.user.tenant_owner;
            },
            /*
             * @ngInject
             */
            integerate: function ($actions) {
                $actions.group('navigationPathMenu').clear();
                $actions.newAction({
                    id: 'tenant',
                    title: 'Tenant Info',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/tenants/current',
                    groups: ['navigationPathMenu']
                });
                $actions.newAction({
                    id: 'tenant-tickets',
                    title: 'Tickets',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/tickets',
                    groups: ['navigationPathMenu']
                });
            }
        }) //
        .when('/tenant/tickets/:ticketId', {
            templateUrl: 'views/amd-tenant/ticket.html',
            controller: 'AmdTenantTicketController',
            /*
             * @ngInject
             */
            protect: function ($rootScope) {
                return !$rootScope.app.user.tenant_owner;
            },
            /*
             * @ngInject
             */
            integerate: function ($actions, $routeParams) {
                $actions.group('navigationPathMenu').clear();
                $actions.newAction({
                    id: 'tenant',
                    title: 'Tenant Info',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/tenants/current',
                    groups: ['navigationPathMenu']
                });
                $actions.newAction({
                    id: 'tenant-tickets',
                    title: 'Tickets',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/tickets',
                    groups: ['navigationPathMenu']
                });
                $actions.newAction({
                    id: 'tenant-ticket-' + $routeParams.ticketId,
                    title: 'Ticket #' + $routeParams.ticketId,
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/tickets/' + $routeParams.ticketId,
                    groups: ['navigationPathMenu']
                });
            }
        })
        .when('/tenant/invoices', {
            templateUrl: 'views/amd-tenant/invoice-list.html',
            controller: 'AmdTenantInvoicesController',
            groups: ['tenant'],
            navigate: true,
            name: 'Invoices',
            icon: 'attach_money',
            helpId: 'invoice',
            /*
             * @ngInject
             */
            protect: function ($rootScope) {
                return !$rootScope.__account.permissions.tenant_owner;
            },
            /*
             * @ngInject
             */
            integerate: function ($actions) {
                $actions.group('navigationPathMenu').clear();
                $actions.newAction({
                    id: 'tenant',
                    title: 'Tenant Info',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/tenants/current',
                    groups: ['navigationPathMenu']
                });
                $actions.newAction({
                    id: 'tenant-invoices',
                    title: 'Invoices',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/invoices',
                    groups: ['navigationPathMenu']
                });
            }
        }) //
        .when('/tenant/invoices/new', {
            templateUrl: 'views/amd-tenant/invoice-new.html',
            controller: 'AmdTenantInvoiceController',
            /*
             * @ngInject
             */
            protect: function ($rootScope) {
                return !$rootScope.__account.permissions.tenant_owner;
            },
            /*
             * @ngInject
             */
            integerate: function ($actions) {
                $actions.group('navigationPathMenu').clear();
                $actions.newAction({
                    id: 'tenant',
                    title: 'Tenant Info',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/tenants/current',
                    groups: ['navigationPathMenu']
                });
                $actions.newAction({
                    id: 'tenant-invoices',
                    title: 'Invoices',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/invoices',
                    groups: ['navigationPathMenu']
                });
                $actions.newAction({
                    id: 'new-invoice',
                    title: 'New invoice',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/invoices/new',
                    groups: ['navigationPathMenu']
                });
            }
        }) //
        .when('/tenant/invoices/:invoiceId', {
            templateUrl: 'views/amd-tenant/invoice.html',
            controller: 'AmdTenantInvoiceController',
            helpId: 'invoiceDetails',
            /*
             * @ngInject
             */
            protect: function ($rootScope) {
                return !$rootScope.__account.permissions.tenant_owner;
            },
            /*
             * @ngInject
             */
            integerate: function ($actions, $routeParams) {
                $actions.group('navigationPathMenu').clear();
                $actions.newAction({
                    id: 'tenant',
                    title: 'Tenant Info',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/tenants/current',
                    groups: ['navigationPathMenu']
                });
                $actions.newAction({
                    id: 'tenant-invoices',
                    title: 'Invoices',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/invoices',
                    groups: ['navigationPathMenu']
                });
                $actions.newAction({
                    id: 'tenant-invoice-' + $routeParams.invoiceId,
                    title: 'Ticket #' + $routeParams.invoiceId,
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/invoices/' + $routeParams.invoiceId,
                    groups: ['navigationPathMenu']
                });
            }
        }) //
        .when('/receipts/:id', {
            templateUrl: 'views/amd-tenant-receipt.html',
            controller: 'AmdTenantReceiptCtrl',
            protect: true,
            helpId: 'receipt'
        })

        .when('/tenant/settings/security', {
            templateUrl: 'views/amd-setting-security.html',
            controller: 'AmdSettingsSecurityCtrl',
            navigate: true,
            groups: ['tenant'],
            name: 'Security',
            icon: 'font_download',
            helpId: 'setting-captcha',
            /*
             * @ngInject
             */
            protect: function ($rootScope) {
                return !$rootScope.__account.permissions.tenant_owner;
            },
            /*
             * @ngInject
             */
            integerate: function ($actions) {
                $actions.group('navigationPathMenu').clear();
                $actions.newAction({
                    id: 'tenant',
                    title: 'Tenant Info',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/tenants/current',
                    groups: ['navigationPathMenu']
                });
                $actions.newAction({
                    id: 'tenant-settings',
                    title: 'Tenant Settings',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/settings',
                    groups: ['navigationPathMenu']
                });
                $actions.newAction({
                    id: 'tenant-security',
                    title: 'Security',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/settings/security',
                    groups: ['navigationPathMenu']
                });
            }
        }).when('/tenant/settings/local-setting', {
            templateUrl: 'views/amd-local-setting.html',
            controller: 'AmdLocalSettingsCtrl',
            navigate: true,
            groups: ['tenant'],
            name: 'Local settings',
            icon: 'settings_applications',
            helpId: '',
            /*
             * @ngInject
             */
            protect: function ($rootScope) {
                return !$rootScope.__account.permissions.tenant_owner;
            },
            /*
             * @ngInject
             */
            integerate: function ($actions) {
                $actions.group('navigationPathMenu').clear();
                $actions.newAction({
                    id: 'tenant',
                    title: 'Tenant Info',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/tenants/current',
                    groups: ['navigationPathMenu']
                });
                $actions.newAction({
                    id: 'tenant-settings',
                    title: 'Tenant Settings',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/settings',
                    groups: ['navigationPathMenu']
                });
                $actions.newAction({
                    id: 'local-settings',
                    title: 'Local settings',
                    type: 'link',
                    priority: 10,
                    visible: true,
                    url: 'tenant/settings/local-settings',
                    groups: ['navigationPathMenu']
                });
            }
        });
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardTenant')

/**
 * @ngdoc controller
 * @name AmdTenantInvoiceController
 * @description Manages view of a invoice
 */
.controller('AmdTenantInvoiceController', function($scope, $rootScope, $routeParams, $location, 
		/*$discount,*/ $tenant) {

	/**
	 * Controller status
	 */
//        $scope.amdDisableId = true;
	var status = {
		invoiceLoading : false,
		gateLoading: false,
		success: true
	};
	var ctrl = {};
	$scope.ctrl = ctrl;
	$scope.status = status;

	function checkState(myInvoice){
		return myInvoice.state()//
		.then(function(inv){
			$scope.status.invoiceLoading = false;
			$scope.invoice = inv;
//			$scope.translateParams.expirationDate = pdateFilter(link.expiry, 'jYYYY/jMM/jDD hh:mm');
		});
	}
	
	function loadGates(){
		$scope.status.gateLoading = true;
		return $tenant.getGates()//
		.then(function(gatesPag){
			$scope.gates = gatesPag.items;
			$scope.status.gateLoading = false;
			return gatesPag;
		});
	}
	
	function pay(backend, discountCode){
		// create receipt and send to bank receipt page.
		var data = {
			'backend' : backend.id,
			'callback' : $location.absUrl()
		};
		if(typeof discountCode !== 'undefined' && discountCode !== null){
			data.discount_code = discountCode;
		}
		$scope.invoice.pay(data)//
		.then(function(receipt){
			goTo('/receipts/' + receipt.id);
		}, handleException);
	}

	/**
	 * به صفحه دیگر با آدرس داده شده می‌رود
	 */
	function goTo(path) {
		$location.path(path);
	}
	
	function handleException(error){
		if(error.status === 401 && $rootScope.app.user.anonymous){
			$scope.goTo('/users/login');
			return;
		}
		$scope.error = error.message;
		$scope.status.invoiceLoading = false;
		$scope.status.gateLoading = false;
		$scope.status.success = false;
	}
	
	function checkDiscount(code){
		// XXX: hadi 1396-11-05: should be used $tenant service to compute discount. $discount service is for other tenants. 
		// XXX: hadi 1396-11-05: RESTs for discounts of super tenant should be added to $tenant service.  
		
//		$discount.discount(code)//
//		.then(function(discount){
//			if(typeof discount.validation_code === 'undefined' || discount.validation_code === 0){
//				$scope.discount_message = 'discount is valid';
//			}else{
//				switch(discount.validation_code){
//				case 1:
//					$scope.discount_message = 'discount is used before';
//					break;
//				case 2: 
//					$scope.discount_message = 'discount is expired';
//					break;
//				case 3: 
//					// discount is not owned by user.
//					$scope.discount_message = 'discount is not valid';
//					break;
//				}
//			}
//		}, function(error){
//			$scope.error = error.data.message;
//			if(error.status === 404){				
//				$scope.discount_message = 'discount is not found';
//			}else{
//				$scope.discount_message = 'unknown error while get discount info';
//			}
//		});
	}

	/**
	 * Load ticket
	 */
	function loadInvoice(){
		if($scope.status.invoiceLoading === true){
			return;
		}
		$scope.status.invoiceLoading = true;
		$scope.invoice = null;
		ctrl.items = [];
		return $tenant.getInvoice($routeParams.invoiceId)//
		.then(function(inv){
			$scope.invoice = inv;
			status.invoiceLoading = false;
			return $scope.invoice;
		}, function(error){
			status.invoiceLoading = false;
			ctrl.error = error;
		});
	}

	function load() {

		// load invoice
		loadInvoice()
		// check state of invoice
		.then(checkState)
		// load gates
		.then(loadGates)
		// handle exceptions
		.catch(handleException);
				
	}
	
	$scope.hostUrl = $location.protocol() + '://' + $location.host();
	$scope.pay = pay;
	$scope.checkDiscount = checkDiscount;
	
	load();
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardTenant')

/**
 */
.controller('AmdTenantInvoicesController', function($scope, $tenant, $translate, QueryParameter) {

	var paginatorParameter = new QueryParameter();
	var requests = null;
	var ctrl = {
		items: []
	};


	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.loadingInvoices || (requests && !requests.hasMore())) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		ctrl.loadingInvoices = true;
		$tenant.getInvoices(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
		}, function() {
			alert($translate.instant('Failed to load invoices.'));
		})
		.finally(function(){
			ctrl.loadingInvoices = false;
		});
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload() {
		requests = null;
		ctrl.items = [];
		nextPage();
	}


	$scope.ctrl = ctrl;
	$scope.nextPage = nextPage;
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.sortKeys = [ 'id', 'title', 'amount', 'due_dtime', 'creation_dtime' ];
	$scope.sortKeysTitles = [ 'Id', 'Title', 'Amount', 'Due time', 'creation time'];

});




angular.module('ngMaterialDashboardTenant')

	/**
	 * @ngdoc controller
	 * @name AmdLocalSettingsCtrl
	 * @description Setting controller for local settings
	 */
	.controller('AmdLocalSettingsCtrl', function ($scope, $tenant, $q, $http) {

		var ctrl = {
			loadingSettings: false,
			savingSettings: false
		};

		var settingKeys = [
			'local.language',
			'local.date',
			'local.currency'
		];

		$scope.settings = {};
		$scope.languages = { 'fa': 'Persian', 'en': 'English' };
		$scope.dates = { 'jalali': 'Jalali', 'gergorian': 'Gergorian' };

		ctrl.currencies = {};

		function loadCurrencies() {
			$http({
				method: 'GET',
				url: 'https://openexchangerates.org/api/currencies.json'
			}).then(function (response) {
				var currencies = response.data;
				// Add Toman to the list
				var newCurrency = Object.assign({ IRT: 'Iranian Toman' }, currencies);
				//sort based on keys
				Object.keys(newCurrency).sort().forEach(function (key) {
					ctrl.currencies[key] = newCurrency[key];
				});
			});
		}

		function fetchSetting(key) {
			return $tenant.getSetting(key)
				.then(function (sett) {
					return sett;
				}, function (error) {
					if (error.status === 404) {
						return $tenant.putSetting({
							'key': key,
							'value': ''
						});
					} else {
						throw error;
					}
				})//
				.then(function (settingItem) {
					$scope.settings[key] = settingItem;
				});
		}

		function save() {
			ctrl.savingSettings = true;
			var promiseList = []
			for (var i = 0; i < settingKeys.length; i++) {
				var key = settingKeys[i];
				if (angular.isDefined($scope.settings[key]) && angular.isFunction($scope.settings[key].update)) {
					promiseList.push($scope.settings[key].update());
				} else if (angular.isDefined($scope.settings[key].value)) {
					promiseList.push($tenant.putSettings({ 'key': key, 'value': $scope.settings[key].value }));
				}
			}
			$q.all(promiseList)//
				.finally(function () {
					ctrl.savingSettings = false;
					toast('Settings saved successfully');
				});
		}

		function loadSettings() {
			ctrl.loadingSettings = true;
			var promiseList = [];
			for (var i = 0; i < settingKeys.length; i++) {
				var key = settingKeys[i];
				promiseList.push(fetchSetting(key));
			}
			$q.all(promiseList)//
				.finally(function () {
					ctrl.loadingSettings = false;
				});
		}

		$scope.ctrl = ctrl;
		$scope.save = save;

		loadSettings();
		loadCurrencies();
	});



/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialDashboardTenant')


	/**
	 *
	 */
	.controller('AmdTenantReceiptCtrl', function ($scope, $routeParams, $tenant, $app, $window) {

		var ctrl = {
			laoding: false,
			receipt: null,
			gate: null
		};
		$scope.ctrl = ctrl;

		/**
		 * Sets a receipt in the scope
		 *
		 * @param receipt
		 * @returns
		 */
		function setReceipt(receipt) {
			ctrl.receipt = receipt;
			// TODO: set page title
		}

		/**
		 * Get the receipt id
		 * @returns
		 */
		function getReceiptId() {
			return ($routeParams.id || null);
		}

		/**
		 * Loads receipt data
		 * @returns
		 */
		function loadReceipt() {
			if (ctrl.loading === true) {
				return;
			}
			ctrl.loading = true;
			$tenant.getReceipt(getReceiptId())//
				.then(function (receipt) {
					setReceipt(receipt);
					return $tenant.getGate(receipt.backend);
				}, function (error) {
					ctrl.error = error;
				})//
				.then(function (gate) {
					ctrl.gate = gate;
				})//
				.finally(function () {
					ctrl.loading = false;
				});
		}

		/**
		 * Cancel page
		 * @returns
		 */
		//	function cancel(){
		//		$window.history.back();
		//	}

		$scope.load = loadReceipt;
		//	loadReceipt();
	});


angular.module('ngMaterialDashboardTenant')

	/**
	 * @ngdoc controller
	 * @name AmdSettingsCaptchaCtrl
	 * @description Setting controller for captcha
	 * 
	 * This is a controller to manage server side setting of capthca engine. In this
	 * version following engines are supported:
	 * 
	 * <ul>
	 * <li>No captcha</li>
	 * <li>Google reCaptcha</li>
	 * </ul>
	 * 
	 */
	.controller('AmdSettingsSecurityCtrl', function ($scope, $tenant, $q) {

		$scope.options = ["nocaptcha", "recaptcha"];

	    /**
	     * Gets information of a setting with given key. If there is no such setting
	     * item it creates a setting item with given key.
	     * 
	     * @param key
	     *            the key of setting item
	     */
		function _fetchSettingItem(key) {
			return $tenant.getSetting(key)
				.then(function (sett) {
					return sett;
				}, function (error) {
					if (error.status === 404) {
						return $tenant.putSetting({
							'key': key,
							'value': '',
							'mode': 0,
							'description': ''
						});
					} else {
						throw error;
					}
				})//
				.then(function (settingItem) {
					return settingItem;
				});
		}

	    /**
	     * Loads options of a captcha engine
	     * 
	     * @param engine
	     *            name of captcha engine
	     */
		function _loadCaptchaOptions(engine) {
			if (engine === 'recaptcha') {
				$scope.loadingCaptchaOptions = true;

				var p1 = _fetchSettingItem('captcha.engine.recaptcha.key')
					.then(function (res) {
						$scope.keySetting = res;
					});
				var p2 = _fetchSettingItem('captcha.engine.recaptcha.secret')
					.then(function (res) {
						$scope.secretKeySetting = res;
					});
				var p3 = _fetchSettingItem('captcha.engine.recaptcha.android.key')
					.then(function (res) {
						$scope.androidKeySetting = res;
					});
				var p4 = _fetchSettingItem('captcha.engine.recaptcha.android.secret')
					.then(function (res) {
						$scope.androidSecretKeySetting = res;
					});

				$q.all(p1, p2, p3, p4)//
					.finally(function () {
						$scope.loadingCaptchaOptions = false;
					});
			}
		}

		function loadCaptchaSetting() {
			$scope.loadingCaptchaSettings = true;
			_fetchSettingItem('captcha.engine')//
				.then(function (res) {
					$scope.captcha = res;
					_loadCaptchaOptions(res.value);
				}, function (error) {
					alert(error);
				})//
				.finally(function () {
					$scope.loadingCaptchaSettings = false;
				});
		}

		function loadHttpsSetting() {
			$scope.loadingHttpsSettings = true;
			var p1 = _fetchSettingItem('https.enable')//
				.then(function (res) {
					$scope.httpsEnable = res;
					$scope.enable = (res.value === 'true');
				});
			var p2 = _fetchSettingItem('https.redirect')//
				.then(function (res) {
					$scope.httpsRedirect = res;
					$scope.redirect = (res.value === 'true');
				});
			$q.all(p1, p2)
				.finally(function () {
					$scope.loadingHttpsSettings = false;
				});
		}

		function saveSetting() {
			$scope.savingSettings = true;
			var p1, p2, p3, p4, p5, p6, p7;

			p1 = $scope.captcha.update();
			if ($scope.captcha.value === 'recaptcha') {

				if ($scope.keySetting.value) {
					p2 = $scope.keySetting.update();
				}
				if ($scope.secretKeySetting.value) {
					$scope.secretKeySetting.mode = 0;
					p3 = $scope.secretKeySetting.update();
				}
				if ($scope.androidKeySetting.value) {
					p4 = $scope.androidKeySetting.update();
				}
				if ($scope.androidSecretKeySetting.value) {
					$scope.androidSecretKeySetting.mode = 0;
					p5 = $scope.androidSecretKeySetting.update();
				}
			}

			p6 = $scope.httpsEnable.update();
			p7 = $scope.httpsRedirect.update();

			$q.all(p1, p2, p3, p4, p5, p6, p7)//
				.finally(function () {
					$scope.savingSettings = false;
				});
		}

		function httpsEnableChanged(val) {
			$scope.httpsEnable.value = val;
		}

		function httpsRedirectChanged(val) {
			$scope.httpsRedirect.value = val;
		}

		$scope.$watch(function () {
			if (angular.isDefined($scope.captcha)) {
				return $scope.captcha.value;
			}
			return null;
		}, function (engine) {
			_loadCaptchaOptions(engine);
		});

		$scope.update = saveSetting;
		$scope.httpsEnableChanged = httpsEnableChanged;
		$scope.httpsRedirectChanged = httpsRedirectChanged;

		loadCaptchaSetting();
		loadHttpsSetting();
	});



/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('ngMaterialDashboardTenant')

	/**
	 * @ngdoc Controller
	 * @name AmdTenantTenantController
	 * @description Manage a tenant
	 */
	.controller('AmdTenantTenantController', function (
		/* angularjs   */ $scope, $routeParams, $q,
		/* ngtranslate */ $tenant, $navigator, $translate,
		/* am-wb-core  */ $resource,
		/* seen-tenant */ TenantTenant, TenantAccount) {


	    /**
	     * Load tenant
	     */
		this.load = function () {
			if (this.loading) {
				return;
			}
			var tenantId = $routeParams.tenantId;
			this.loading = true;
			var ctrl = this;
			return $tenant.getTenant(tenantId, {
				graphql:'{id,title,subdomain,domain,modif_dtime,creation_dtime,owners{id,login,date_joined}}'
			})//
				.then(function (tenant) {
					// TODO: maso, 2020: load owners
					ctrl.loadOwners(tenant.owners);
					delete tenant.owners;
					ctrl.tenant = new TenantTenant(tenant);
				}, function (error) {
					alert($translate.instant('Failed to load tenant.'));
				})//
				.finally(function () {
					ctrl.loading = false;
				});
		};

	    /**
	     * Update current tenant
	     */
		this.update = function () {
			if (this.saving) {
				return;
			}
			this.saving = true;
			var ctrl = this;
			this.checkTenant();
			return this.tenant.update()//
				.then(function (tenant) {
					ctrl.tenant = tenant;
					ctrl.edit = false;
				}, function () {
					// show error
					alert($translate.instant('Failed to save tenant'));
				})
				.finally(function () {
					ctrl.saving = false;
				});
		};

		this.checkTenant = function () {
			if (this.tenant.title.length === 0) {
				delete this.tenant.title;
			}
			if (this.tenant.subdomain.length === 0) {
				delete this.tenant.subdomain;
			}
			if (this.tenant.domain.length === 0) {
				delete this.tenant.domain;
			}
		};

	    /**
	     * Remoe current tenant
	     */
		this.remove = function () {
			if (this.removing) {
				return;
			}
			var ctrl = this;
			confirm($translate.instant('delete tenant?'))//
				.then(function () {
					ctrl.removing = true;
					return ctrl.tenant.delete();
				}, function () {
					// cancel action. do nothing.
				})
				.then(function () {
					$navigator.openPage('tenant/tenants');
				}, function () {
					// show error
					alert($translate.instant('Failed to delete tenant'));
				})
				.finally(function () {
					ctrl.removing = false;
				});
		};

		this.addOwners = function(){
			if(this.ownersPromise){
				return this.ownersPromise;
			}
			var ctrl = this;
			$resource.get('/user/accounts')
			.then(function(accounts){
				var jobs = [];
				_.forEach(accounts, function(account){
					jobs.push(ctrl.tenant.putOwner(account)
					.then(function(){
						ctrl.owners.push(account);
					}));
				});
				return ctrl.ownersPromise = $q.all(jobs);
			})
			.finally(function(){
				delete ctrl.ownersPromise;
			});
		};

		this.deleteOwner = function(owner){
			if(this.ownersPromise){
				return this.ownersPromise;
			}
			var ctrl = this;
			this.tenant.deleteOwner(owner)
			.then(function(){
				_.remove(ctrl.owners, function(item){
					return item.id === owner.id;
				});
			})
			.finally(function(){
				delete ctrl.ownersPromise;
			});
		};

		this.loadOwners = function(ownersData){
			var owners = [];
			_.forEach(ownersData, function(ownerData){
				owners.push(new TenantAccount(ownerData));
			});
			this.owners = owners;
		};

		this.load();
	});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialDashboardTenant')

	/**
	 * @ngdoc Controller
	 * @name AmdTenantTenantsController
	 * @description Manages list of tenants from the current tenant.
	 * 
	 * Each tenant can create multi sub tenants. This is a controller of the subtenatns.
	 */
	.controller('AmdTenantTenantsController', function (
		/* angularjs      */ $scope, $controller,
		/* seen-tenant    */ $tenant,
		/* ng-translate   */ $translate,
		/* mblowfish-core */ $navigator) {

		// Extends with ItemsController
		angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
			$scope: $scope
		}));

	    /**
	     * Gets schema of the tenant model
	     */
		this.getModelSchema = function () {
			return $tenant.tenantSchema();
		};

		// get tenants
		this.getModels = function (parameterQuery) {
			return $tenant.getTenants(parameterQuery);
		};

		// get a tenant
		this.getModel = function (id) {
			return $tenant.getTenant(id);
		};

		// delete tenant
		this.deleteModel = function (item) {
			return item.delete();
		};

		this.add = function () {
			return $navigator.openDialog({
				templateUrl: 'views/dialogs/tenant-new.html',
				config: {}
			}).then(function (newConfig) {
				ctrl.createTenant(newConfig);
			});
		};

	    /*
	     * Create tenant 
	     */
		this.createTenant = function (tenant) {
			if (ctrl.tenantSaving) {
				return;
			}
			ctrl.tenantSaving = true;
			$tenant.putTenant(tenant)
				.then(function (tenant) {
					ctrl.items = ctrl.items.concat(tenant);
				}, function () {
					alert($translate.instant('Fail to create new tenant.'));
				})//
				.finally(function () {
					ctrl.tenantSaving = false;
				});
		};


		// initial the controller
		this.init({
			eventType: '/tenant/tenants'
		});

		// add actions
		var ctrl = this;
		this.addActions([{
			title: 'New tenant',
			icon: 'add',
			action: function () {
				ctrl.add();
			}
		}]);
	});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardTenant')

/**
 * 
 * ctrl is placed in scpoe
 * 
 * 
 */
.controller('AmdTenantTicketController', function ($scope, $routeParams, $tenant, QueryParameter) {

    var paginatorParameter = new QueryParameter();
    paginatorParameter.setOrder('creation_dtime', 'd');
    var requests = null;

    /**
     * Controller status
     */
    var ctrl = {
            /**
             * Status of the controller
             * 
             * <ul>
             * <li>relax: all things is ok</li>
             * <li>loading: getting data from server</li>
             * <li>error: faced with error, the error will be placed in ctrl.error</li>
             * </ul>
             */
            status: 'relax',
            /**
             * Comments status
             * 
             * <ul>
             * <li>relax: all things is ok</li>
             * <li>loading: getting data from server</li>
             * <li>error: faced with error, the error will be placed in ctrl.commentsError</li>
             * </ul>
             */
            commentsStatus: 'loading',
            /**
             * List of all comments
             */
            comments: []
    }
    $scope.ctrl = ctrl;

    /**
     * Load ticket
     */
    function loadTicket() {
        if (ctrl.status == 'loading') {
            return;
        }
        $scope.ticket = null;
        requests = null;
        ctrl.items = [];
        ctrl.status = 'loading';
        return $tenant.getTicket($routeParams.ticketId)//
        .then(function (ticket) {
            $scope.ticket = ticket;
            ctrl.status = 'relax';
            ctrl.commentsStatus = 'relax';
            nextPage();
        }, function (error) {
            ctrl.status = 'error';
            ctrl.error = error;
        });
    }

    /**
     * لود کردن داده‌های صفحه بعد
     * 
     * @returns
     */
    function nextPage() {
        if (ctrl.commentsStatus === 'loading') {
            return;
        }
        if (requests && !requests.hasMore()) {
            return;
        }
        if (requests) {
            paginatorParameter.setPage(requests.next());
        }
        // start state (device list)
        ctrl.commentsStatus = 'loading';
        return $scope.ticket.getComments(paginatorParameter)//
        .then(function (items) {
            requests = items;
            ctrl.comments = ctrl.items.concat(requests.items);
            ctrl.commentsStatus = 'relax';
            ctrl.commentsError = null;
        }, function (error) {
            ctrl.commentsError = error;
            ctrl.commentsStatus = 'error';
        });
    }

    /**
     * To add new comment
     */
    function addComment(comment) {
        return $scope.ticket.putComment(comment)//
        .then(function (nc) {
            ctrl.comments.unshift(nc);
        });
    }

    function toggleEdit() {
        $scope.editing = !$scope.editing;
    }

    /**
     * To save the subject
     */
    function updateSubject() {
        $scope.ticket.update()
        .then(function (ticket) {
            $scope.editing = false;
            $scope.ticket.subject = ticket.subject;
        });
    }

    loadTicket();
    $scope.addComment = addComment;
    $scope.toggleEdit = toggleEdit;
    $scope.updateSubject = updateSubject;
    $scope.editing = false;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardTenant')

/**
 */
.controller('AmdTenantTicketsController', function ($scope, $tenant, $navigator, QueryParameter) {

    var paginatorParameter = new QueryParameter();
    var requests = null;
    var ctrl = {
            state: 'relax',
            items: []
    };

    /**
     * لود کردن داده‌های صفحه بعد
     * 
     * @returns
     */
    function nextPage() {
        if (ctrl.status === 'working') {
            return;
        }
        if (requests && !requests.hasMore()) {
            return;
        }
        if (requests) {
            paginatorParameter.setPage(requests.next());
        }
        // start state (device list)
        ctrl.status = 'working';
        $tenant.getTickets(paginatorParameter)//
        .then(function (items) {
            requests = items;
            ctrl.items = ctrl.items.concat(requests.items);
            ctrl.status = 'relax';
        }, function (error) {
            ctrl.status = 'fail';
            ctrl.error = error;
        });
    }

    /**
     * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
     * 
     * @returns
     */
    function reload() {
        requests = null;
        ctrl.items = [];
        nextPage();
    }

    /**
     * Adds new ticket
     * 
     * @memberof AmdTenantTicketsController
     */
    function add() {
        // get subject of new ticket
        return $navigator.openDialog({
            templateUrl : 'views/dialogs/subject.html',
            model : {
                subject : 'subject'
            }
        })
        .then(function (subject) {
            return $tenant.putTicket({
                subject: subject
            })//
        })
        .then(function (ticket) {
            $navigator.openPage('tenant/tickets/' + ticket.id);
        });//
    }
    ;
    function remove() {}
    function open() {}


    /*
     * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری
     * شده است.
     */
    $scope.items = [];
    $scope.nextPage = nextPage;
    $scope.ctrl = ctrl;

    $scope.add = add;
    $scope.remove = remove;
    $scope.open = open;

    $scope.paginatorParameter = paginatorParameter;
    $scope.reload = reload;
    $scope.sortKeys = ['id', 'creation_dtime'];
    $scope.moreActions = [{
        title: 'New ticket',
        icon: 'add',
        action: add
    }];
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardTenant')
.directive('amdTenantInvoice', function(){
    function postLink($scope,$element,$attributes,$ctrls){
       var ngModel = $ctrls[0] ;
       ngModel.$render = function(){
           $scope.invoice = ngModel.$modelValue;
       };
    }
    return {
      restrict: 'E',
      templateUrl: 'views/amd-directives/amd-tenant-invoice.html',
      scope: {
        disableId: '<amdDisableId',
        title: '@'
      },
      link : postLink,
/*    controller: 'the name of a controller defined in controllers',
 * or directly -> controller: function(){
 *                      define every ctrls to act on view
 *                  }             */

      require : ['ngModel']
    };
  });
  

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('ngMaterialDashboardTenant')

.filter('iranCurrency', function(numberFilter, translateFilter) {
	return function(number, unit) {
		var txt = '';
		if (!number){
			return translateFilter('free');
		}
		if (unit === 'R') {
			txt = 'rial';
		} else if (unit === 'T') {
			txt = 'tooman';
		}
		return numberFilter(number) + ' ' + translateFilter(txt);
	};
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardTenant')
.run(function ($navigator, $app) {
    $navigator.newGroup({
        id: 'tenant',
        title: 'Tenant',
        description: 'A module of dashboard to manage tenants.',
        icon: 'web',
        hidden: function ($rootScope) {
            return !($rootScope.app.user.tenant_owner/*  && $app.isEnable('tenant') */);
        },
        priority: 5
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardTenant')
/**
 * دریچه‌های محاوره‌ای
 */
.run(function() {
//	// Settings
//	$options//
//	.newPage({
//		title: 'Messages',
//		templateUrl: 'views/amd-account-settings/messages.html',
//		controller: 'MessagesCtrl',
//		tags: ['messages', 'account']
//	});
});
angular.module('ngMaterialDashboardTenant').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-directives/amd-tenant-invoice.html',
    "<table class=my-table> <caption class=md-accent translate><h2>{{title}}</h2></caption> <tr ng-show=!disableId><td translate>ID</td><td> {{invoice.id}}</td></tr> <tr><td translate>Title</td><td> {{invoice.title}}</td></tr> <tr><td translate>Amount</td><td> {{invoice.amount}}</td></tr> <tr><td translate>Status</td><td> {{invoice.status}}</td></tr> <tr><td translate>Due Date</td><td> {{invoice.due_time}}</td></tr> <tr><td translate>Creation Datetime</td><td> {{invoice.creation_dtime | mbDate}}</td></tr>  </table>"
  );


  $templateCache.put('views/amd-local-setting.html',
    "<md-content flex layout=column layout-padding mb-preloading=\"ctrl.loadingSettings || ctrl.savingSettings\"> <md-input-container> <label translate>Language</label> <md-select ng-model=\"settings['local.language'].value\"> <md-option ng-value=lang ng-repeat=\"lang in languages\"> <span translate>{{lang}}</span> </md-option> </md-select> </md-input-container> <md-input-container> <label translate>Date format</label> <md-select ng-model=\"settings['local.date'].value\"> <md-option ng-value=date ng-repeat=\"date in dates\"> <span translate>{{date}}</span> </md-option> </md-select> </md-input-container> <md-input-container> <label translate>Currency</label> <md-select ng-model=\"settings['local.currency'].value\"> <md-option ng-value=key ng-repeat=\"(key,value) in ctrl.currencies\"> <span translate>{{value}}</span> </md-option> </md-select> </md-input-container> <div> <div layout=row> <span flex></span> <md-button class=md-raised ng-href=\"/\" translate>Cancel</md-button> <md-button class=\"md-raised md-primary\" ng-click=save() translate>Save</md-button> </div> </div> </md-content>"
  );


  $templateCache.put('views/amd-setting-security.html',
    "<md-content flex layout=column layout-padding> <mb-titled-block ng-show=!ctrl.edit mb-progress=\"loadingCaptchaOptions || loadingCaptchaSettings || savingSettings\" mb-title=\"{{'Captcha'| translate}}\" layout=column> <div> <div layout=column layout-align-gt-sm=\"start start\"> <md-select ng-model=captcha.value placeholder=\"Select a state\"> <md-option ng-value=option ng-repeat=\"option in options\"> <span translate=\"\">{{option}}</span> </md-option> </md-select> </div> </div> <div style=\"margin-top: 20px\"> <div layout=column ng-show=\"captcha.value === 'recaptcha'\"> <md-input-container style=\"padding: 0px;margin: 0px\"> <label translate=\"\">Site Key</label> <input ng-model=keySetting.value> </md-input-container> <md-input-container style=\"padding: 0px;margin: 0px\"> <label translate=\"\">Secret Key</label> <input ng-model=secretKeySetting.value> </md-input-container> <md-input-container style=\"padding: 0px;margin: 0px\"> <label translate=\"\">Android Key </label> <input ng-model=androidKeySetting.value> </md-input-container> <md-input-container style=\"padding: 0px;margin: 0px\"> <label translate=\"\">Android Secret Key </label> <input ng-model=androidSecretKeySetting.value> </md-input-container> </div> </div> </mb-titled-block> <span flex=2></span> <mb-titled-block ng-show=!ctrl.edit mb-progress=\"loadingHttpsSettings || savingSettings\" mb-title=\"{{'HTTPS'| translate}}\" layout=column> <div layout=column style=\"margin-top: 20px\" layout-align-gt-sm=\"start start\"> <md-checkbox ng-model=enable ng-change=httpsEnableChanged(enable) aria-label=\"Enable HTTPS protocol\"> <span translate=\"\">Enable HTTPS</span> </md-checkbox> <md-checkbox ng-model=redirect ng-change=httpsRedirectChanged(redirect) aria-label=\"Redirect HTTP to HTTPS\"> <span translate=\"\">Redirect HTTP to HTTPS</span> </md-checkbox> </div> </mb-titled-block> <div> <div layout=row> <span flex></span> <md-button class=md-raised ng-href=dashboard translate=\"\">Cancel</md-button> <md-button class=\"md-raised md-primary\" ng-click=update() translate=\"\">Save</md-button> </div> </div> </md-content>"
  );


  $templateCache.put('views/amd-tenant-receipt.html',
    "<md-content mb-preloading=ctrl.loading layout-padding flex> <section layout=column layout-align=\"center center\" layout-margin> <table class=receipt-table ng-show=!status.assetLoading> <tbody> <caption translate>Receipt information</caption> <tr> <td translate>Id</td> <td>{{ctrl.receipt.id}}</td> </tr> <tr> <td translate>Title</td> <td>{{ctrl.receipt.title}}</td> </tr> <tr> <td translate>Amount</td> <td>{{ctrl.receipt.amount | iranCurrency:'T'}}</td> </tr> <tr> <td translate>Description</td> <td>{{ctrl.receipt.description}}</td> </tr> <tr> <td translate>Gate</td> <td>  {{ctrl.receipt.backend}} - {{ctrl.gate.title}} </td> </tr> <tr> <td translate>Status</td> <td translate>{{ctrl.receipt.payRef ? 'payed' : 'not payed'}}</td> </tr> </tbody> </table> <md-button ng-disabled=ctrl.receipt.payRef ng-show=!ctrl.receipt.payRef class=\"md-raised md-accent\" ng-href={{ctrl.receipt.callURL}}>{{'complete pay' | translate}}</md-button> </section> </md-content>"
  );


  $templateCache.put('views/amd-tenant-tenant.html',
    "<md-content class=md-padding layout-padding flex> <mb-titled-block mb-progress=\"ctrl.loading || ctrl.removing\" mb-title=\"{{'Tenant'| translate}}\" layout=column> <div layout-padding> <table> <tr> <td translate=\"\">ID</td> <td>: {{::ctrl.tenant.id}}</td> </tr> <tr> <td translate=\"\">Title</td> <td><mb-inline mb-inline-type=text mb-inline-enable={{true}} mb-inline-on-save=ctrl.update() ng-model=ctrl.tenant.title>: {{ctrl.tenant.title}}</mb-inline></td> </tr> <tr> <td translate=\"\">Subdomain</td> <td><mb-inline mb-inline-type=text mb-inline-enable={{true}} mb-inline-on-save=ctrl.update() ng-model=ctrl.tenant.subdomain>: {{ctrl.tenant.subdomain}}</mb-inline></td> </tr> <tr> <td translate=\"\">Domain</td> <td><mb-inline mb-inline-type=text mb-inline-enable={{true}} mb-inline-on-save=ctrl.update() ng-model=ctrl.tenant.domain>: {{ctrl.tenant.domain}}</mb-inline></td> </tr> <tr> <td translate=\"\">Creation Date</td> <td>: {{ctrl.tenant.creation_dtime| mbDate:'jYYYY-jMM-jDD'}}</td> </tr> <tr> <td translate=\"\">Last Modified Date</td> <td>: {{ctrl.tenant.modif_dtime| mbDate:'jYYYY-jMM-jDD'}}</td> </tr> </table> </div> <div ng-show=!ctrl.edit layout-gt-xs=row layout=column> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=ctrl.remove() aria-label=\"Delete tenant\"> <span translate=\"\">Delete</span> </md-button> <md-button class=md-raised ng-href=http://{{ctrl.tenant.domain}} aria-label=\"Delete tenant\"> <span translate=\"\">Go to</span> </md-button> </div> </mb-titled-block> <mb-titled-block mb-progress=ctrl.ownersPromise mb-title=\"{{'Owners'| translate}}\" layout=column> <md-list flex> <md-list-item ng-repeat=\"user in ctrl.owners track by user.id\" ng-href=ums/accounts/{{::user.id}} class=md-3-line> <img class=md-avatar ng-src=/api/v2/user/accounts/{{::user.id}}/avatar ng-src-error=\"https://www.gravatar.com/avatar/{{ ::user.id | wbmd5 }}?d=identicon&size=32\"> <div class=md-list-item-text layout=column> <h3>{{user.profiles[0].first_name}} - {{user.profiles[0].last_name}}</h3> <h4> <span ng-show=user.is_active> <span translate>Active</span>, </span> <span ng-show=!user.is_active> <span translate>Inactive</span>, </span> </h4> <p> <span translate>Joined</span>: {{user.date_joined}}, <span translate>Last Login</span>: {{user.last_login}}, </p> </div> <wb-icon class=\"md-secondary md-hue-3\" aria-label=\"Delete Owner\" ng-click=\"ctrl.deleteOwner(user, $event)\">delete</wb-icon> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=row> <span flex></span> <md-button class=md-icon-button ng-click=ctrl.addOwners()> <wb-icon>add</wb-icon> </md-button> </div> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-tenant-tenants.html',
    "<md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex>  <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.sortKeys mb-more-actions=ctrl.actions mb-properties=ctrl.properties> </mb-pagination-bar>  <div layout=column layout-align=\"start center\"> <md-progress-linear ng-if=\"ctrl.state === 'busy' || ctrl.tenantSaving\" class=md-accent md-diameter=96> {{'Loading ...'| translate}} </md-progress-linear> </div>  <div layout=column ng-if=\"ctrl.state !== 'busy' && (ctrl.items.length === 0 && !ctrl.loadingItems)\"> <p style=\"text-align: center\" translate=\"\">List is empty.</p> <div layout=row layout-align=\"center center\"> <md-button class=md-raised ng-click=ctrl.add()> <wb-icon>add</wb-icon> <span translate>New tenant</span> </md-button> </div> </div> <md-list ng-if=\"ctrl.state !== 'busy' && ctrl.items.length > 0\" flex> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-3-line ng-href=\"{{'tenant/tenants/' + pobject.id}}\" style=\"cursor: pointer\"> <wb-icon>domain</wb-icon> <div class=md-list-item-text layout=column> <h3 ng-if=pobject.title>{{pobject.title}}</h3> <p><span translate=\"\">Subdomain: </span>{{pobject.subdomain}}</p> <p ng-if=pobject.domain><span translate=\"\">Domain: </span>{{pobject.domain}}</p> </div> <md-button class=md-icon-button ng-href=http://{{pobject.domain}} target=_blank> <wb-icon aria-label=\"Go to tenant\">reply</wb-icon> <md-tooltip md-direction=right><span translate=\"\">Go to tenant</span></md-tooltip> </md-button> <md-divider md-inset></md-divider> </md-list-item> </md-list> </md-content>"
  );


  $templateCache.put('views/amd-tenant/current.html',
    "<md-content mb-preloading=\"ctrl.status === 'loading'\" layout=column flex> <div> <md-input-container class=md-block flex-gt-xs> <label translate>ID</label> <input ng-model=tenant.id disabled> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label translate>Title</label> <input ng-model=tenant.title> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label translate>Description</label> <input ng-model=tenant.description> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label translate>Sub Domain</label> <input ng-model=tenant.subdomain disabled> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label translate>Domain</label> <input ng-model=tenant.domain> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label translate>Creation date</label> <input ng-model=tenant.creation_dtime disabled> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label translate>Modification date</label> <input ng-model=tenant.modif_dtime disabled> </md-input-container> </div> <div layout=row> <span flex></span> <md-button class=\"md-accent md-raised\" ng-click=remove()> <wb-icon>delete</wb-icon> <span translate>Delete</span> </md-button> <md-button class=md-raised ng-click=update()> <wb-icon>save</wb-icon> <span translate>Save</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-tenant/invoice-list.html',
    "<md-content flex layout=column mb-infinate-scroll=nextPage() mb-preloading=ctrl.loadingInvoices>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload() mb-sort-keys=sortKeys mb-sort-keys-titles=sortKeysTitles mb-more-actions=moreActions> </mb-pagination-bar>  <md-list flex ng-if=\"!ctrl.loadingInvoices && ctrl.items.length\"> <md-list-item ng-repeat=\"item in ctrl.items\" class=md-3-line ng-href=\"{{'tenant/invoices/'+item.id}}\" md-colors=\"{backgroundColor: item.status==='payed' ? 'background': 'accent-100'}\"> <wb-icon>{{item.status == 'payed' ? 'done' : 'attach_money'}}</wb-icon> <div class=md-list-item-text layout=column> <h3>{{item.title}}</h3> <h4>{{'Amount' | translate}}: {{item.amount}}</h4> <p>{{item.due_dtime | mbDate:'jYYYY-jMM-jDD'}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column ng-if=\"!ctrl.loadingInvoices && !ctrl.items.length\"> <h3 style=\"text-align: center\" translate>Nothing found.</h3> </div> </md-content>"
  );


  $templateCache.put('views/amd-tenant/invoice-new.html',
    ""
  );


  $templateCache.put('views/amd-tenant/invoice.html',
    "<md-content layout=column layout-align=\"center center\" layout-padding flex mb-preloading=\"status.invoiceLoading || status.gateLoading\">  <amd-tenant-invoice amd-disable-id=true title=\"Invoice details\" ng-model=invoice> </amd-tenant-invoice> <div layout=column ng-show=\"status.success && invoice.status !== 'payed'\"> <p translate>Select gate to pay</p> <div layout=column layout-align=\"center center\" ng-if=false> <p>{{error | translate}}</p> </div> <div layout=row layout-align=\"center center\" ng-if=!status.gateLoading> <md-button style=\"text-align: center\" ng-repeat=\"gate in gates\" class=md-raised ng-click=\"pay(gate, discountCode)\"> <md-icon md-svg-src={{gate.symbol}}></md-icon> {{gate.title}} </md-button> </div> </div> <div layout=column layout-align=\"center center\" ng-if=!success> <p>{{error | translate}}</p> </div> </md-content>"
  );


  $templateCache.put('views/amd-tenant/tenant.html',
    "<md-content layout=column layout-padding flex> <md-whiteframe class=md-whiteframe-5dp> <table> <tr><td>ID</td><td>: {{tenant.id}}</td></tr> <tr><td>domain</td><td>: {{tenant.domain}}</td></tr> <tr><td>subdomain</td><td>: {{tenant.subdomain}}</td></tr> <tr><td>creation_dtime</td><td>: {{tenant.creation_dtime}}</td></tr> <tr><td>modif_dtime</td><td>: {{tenant.modif_dtime}}</td></tr> <tr><td>description</td><td>: {{tenant.description}}</td></tr> </table> <md-progress-circular ng-if=\"ctrl.status === 'loading'\" md-diameter=96> </md-progress-circular> </md-whiteframe> <md-whiteframe class=md-whiteframe-5dp> <h4 translate>Owners</h4> <hr> <md-button> <span>Manage</span> </md-button> </md-whiteframe> <md-whiteframe class=md-whiteframe-5dp> <h4 translate>Members</h4> <hr> <md-button> <span>Manage</span> </md-button> </md-whiteframe> <md-whiteframe class=md-whiteframe-5dp> <h4 translate>Authotia</h4> <hr> <md-button> <span>Manage</span> </md-button> </md-whiteframe> </md-content>"
  );


  $templateCache.put('views/amd-tenant/ticket-list.html',
    "<div mb-infinate-scroll=nextPage() layout=column flex>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload() mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar>  <div layout=column layout-align=\"start center\"> <md-progress-linear ng-show=\"ctrl.status === 'working'\" class=md-accent md-diameter=96> {{'Loading ...'| translate}} </md-progress-linear> </div> <div layout=column ng-if=\"ctrl.status !== 'working' && ctrl.items.length === 0\"> <h3 style=\"text-align: center\" translate>Item not found</h3> <div layout=row layout-align=\"center center\"> <md-button ng-click=add()> <wb-icon>add</wb-icon> <span translate>add</span> </md-button> </div> </div> <div ng-show=\"ctrl.status !== 'working'\" layout=row>  <md-list ng-if=\"ctrl.items.length > 0\" flex flex-gt-sm=30 style=\"background-color: #dcd8d8;height: 100vh\"> <md-list-item ng-repeat=\"item in ctrl.items\" class=md-3-line ng-href=\"{{'tenant/tickets/' + item.id}}\"> <div class=md-list-item-text layout=column> <h3>{{item.subject}}</h3> <h4>{{item.description}}</h4> <p>{{item.creation_dtime}}, <span translate=\"\">User</span>:{{item.requester}}</p> </div> </md-list-item> </md-list>  <div id=tickets-list-left-panel layout=column flex layout-align=\"center center\"> <img width=80px height=80px ng-src=resources/images/chat.gif class=\"md-avatar\"> <p translate=\"\">Select a ticket</p> </div> </div> </div>"
  );


  $templateCache.put('views/amd-tenant/ticket-new.html',
    ""
  );


  $templateCache.put('views/amd-tenant/ticket.html',
    "<div layout=row flex> <div id=tickets-list ng-controller=AmdTenantTicketsController ng-init=nextPage() layout=column flex-gt-sm=30 style=\"background-color: #dcd8d8;height: 100vh\">   <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload() mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar> <md-list ng-if=\"ctrl.items.length > 0\" flex> <md-list-item ng-repeat=\"item in ctrl.items\" class=md-3-line ng-href=\"{{'tenant/tickets/' + item.id}}\"> <div class=md-list-item-text layout=column> <h3>{{item.subject}}</h3> <h4>{{item.description}}</h4> <p>{{item.creation_dtime}}, <span translate=\"\">User</span>:{{item.requester}}</p> </div> </md-list-item> </md-list> </div> <md-content layout=column flex> <div ng-if=!editing layout=row layout-align=\"center center\" style=\"min-height: 60px\"> <h2>{{ticket.subject}}</h2> <span flex=5></span> <md-button class=\"md-icon-button md-raised\" ng-click=toggleEdit()> <wb-icon>edit</wb-icon> </md-button> </div> <div ng-if=editing layout=row layout-align=\"center center\" style=\"min-height: 60px\"> <md-input-container class=md-block> <label translate=\"\">Subject</label> <input ng-model=ticket.subject ng-change=updateSubject() ng-model-options=\"{updateOn : 'change blur'}\"> </md-input-container> <md-progress-circular ng-if=\"ctrl.status === 'loading'\" md-diameter=96> </md-progress-circular> </div> <md-divider></md-divider>  <div layout-padding> <md-progress-circular ng-if=\"ctrl.commentsStatus === 'loading'\" md-diameter=96> </md-progress-circular> <div layout=row ng-if=\"ctrl.commentsStatus === 'relax'\"> <div> <img width=68px ng-src=/api/v2/user/accounts/{{app.user.current.id}}/avatar class=\"md-avatar\"> </div> <div class=triangle-topright amd-style-color=\"{'border-top-color': 'accent.100'}\"> </div> <md-whiteframe style=\"margin-bottom: 16px;padding: 10px\" class=md-whiteframe-13dp amd-style-color=\"{'background-color': 'accent.100'}\" layout=column> <md-input-container class=md-block flex-gt-xs> <label>Title</label> <input ng-model=_temp.title> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label>Description</label> <input ng-model=_temp.description> </md-input-container> <div layout=row layout-align=\"center center\"> <md-button class=\"md-raised md-primary\" ng-click=\"addComment(_temp); _temp = {};\" aria-label=\"Add comment\"> <span translate>send</span> </md-button> </div> </md-whiteframe> </div> <div ng-repeat=\"comment in ctrl.comments\" layout=row> <div> <img width=68px ng-src=/api/v2/user/accounts/{{comment.author}}/avatar class=\"md-avatar\"> </div> <div class=triangle-topright amd-style-color=\"{'border-top-color': 'primary.hue-1'}\"> </div> <md-whiteframe style=\"margin-bottom: 16px;padding: 10px\" class=md-whiteframe-13dp md-colors=\"{backgroundColor: 'primary'}\" layout=column layout-align=\"start start\"> <h4 ng-if=comment.title>{{comment.title}}</h4> <p>{{comment.description}}</p> <md-seperator> <div layout=row> <span ng-if=comment.creation_dtime>{{comment.creation_dtime}}</span> <span ng-if=\"comment.creation_dtime !== comment.modif_dtime\"> <wb-icon>edit</wb-icon> {{comment.modif_dtime}} </span> </div> </md-whiteframe> </div> </div> </md-content> </div>"
  );


  $templateCache.put('views/dialogs/subject.html',
    "<md-dialog layout=column ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <span flex></span> <md-button class=md-icon-button ng-click=answer(model.subject)> <wb-icon aria-label=Done>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column layout-padding layout-align=\"center stretch\" flex> <span flex=1></span> <md-input-container class=md-block> <label translate>Subject of ticket</label> <input ng-model=model.subject> </md-input-container> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/tenant-new.html',
    "<md-dialog layout=column ng-cloak flex=50> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">New service</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config) ng-disabled=myForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <form name=myForm ng-action=add(config) layout=column flex> <md-input-container> <label translate=\"\">Title</label> <input ng-model=config.title> </md-input-container> <md-input-container> <label translate=\"\">Sub domain</label> <input ng-model=config.subdomain name=subdomain required> <div ng-messages=myForm.subdomain.$error> <div ng-message=required translate>This field is required.</div> </div> </md-input-container> <md-input-container> <label translate=\"\">Domain</label> <input ng-model=config.domain> </md-input-container> </form> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/test-include.html',
    "<div id=tickets-list-left-panel layout=column flex layout-align=\"center center\"> <img width=80px height=80px ng-src=https://copypastecommissions.net/wp-content/uploads/2015/11/chat-2-icon11.png class=\"md-avatar\"> <p translate=\"\">Select a ticket</p> </div>"
  );

}]);
